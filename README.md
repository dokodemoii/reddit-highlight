# REDDIT HIGHLIGHT

I found the script on some website and I was not able to locate the source. Consider such script an archive of the original. The purpose of this script is to enable new comment highlighting in reddit. Follow the steps below to use this script.

### Install a Userscript Addon
To use the script, download a userscript addon such as Tampermonkey from the browser's addon store. After the addon is downloaded, go to the dashboard of the addon

### Copy and Paste the JS script
Locate the "add script" button on the addon. After you are prompted with an editor, simply copy and paste the script to the editor space and save the script

### Addendum
I also find that the script will only work is if it is coupled with old.reddit.com. Here is what your reddit it looks like after applying such script.

![alt text](./img/reddit.png "Title")